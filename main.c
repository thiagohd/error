#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
#include<ctype.h>
#include<locale.h>

#define TAM 150
#define MAXSTR 15000

#define true 1
#define false 0

typedef int bool;

long long int contaBytesJls(char *nome);
void nextChar(FILE *p);
FILE *abreArq(char *nome, char *modo);
void verifica(char *nome1, char *nome2);
unsigned char toHex(char l, char r);

char look;

/********************************
 *
 *
 ********************************/
int main(int argc, char *argv[])
{
   char nome1[50], nome2[50];
   printf("argc: %d....\n",argc);
   if(argc == 3)
  {
    printf("Starting....\n");
	strcpy(nome1,argv[1]);
	strcpy(nome2,argv[2]);

  }else
  {
     printf("Digite o nome do arquivo.jls: ");
	 scanf("%s",nome1);
     printf("Digite o nome do arquivo.dat: ");
	 scanf("%s",nome2);
  }
  verifica(nome1, nome2);
  return 0;
}

/********************************
 *
 *
 ********************************/
void verifica(char *nome1, char *nome2)
{
    FILE *p1, *p2;
	long long int cont=0, cont2=0;
	unsigned char dado1, dado2, ant , l,r;
	char mask;
	int pos;
	long long int n, error=0;
    p1 = fopen(nome1,"rb");
    if(p1==NULL)
	{
		printf("Não foi passível abrir o arquivo: %s\n",nome1);
		exit(1);
	}
	p2 = fopen(nome2,"rb");
    if(p2==NULL)
	{
		printf("Não foi passível abrir o arquivo: %s\n",nome2);
		exit(1);
	}
	while(!feof(p1))
	{
		dado1 = fgetc(p1);
		cont++;
		if(cont < 25)
        {
		   printf("pos %4lld: 0x%02x\n",cont,0x00ff&dado1);
		}
		else
        {
           printf("pos %4lld: 0x%02x\n",cont,0x00ff&dado1);
           if(cont == 25)
			 printf("\n\n");

           dado1 = fgetc(p1);
           cont++;
					 l = fgetc(p2);
					 r = fgetc(p2);
           dado2 = toHex(l,r);
           cont2++;
           while(!feof(p2))
           {
              if(dado1 != dado2)
              {
                 printf("pos %4lld: 0x%02x 0x%02x erro!\n",cont2,0x00ff&dado1, 0x00ff&dado2 );
                 error++;
              }else
                 printf("pos %4lld: 0x%02x 0x%02x\n",cont2,0x00ff&dado1, 0x00ff&dado2 );
              dado1 = fgetc(p1);
              l = fgetc(p2);
					    r = fgetc(p2);
              if(!feof(p2))
								dado2 = toHex(l,r);
              cont++;
              cont2++;
              
           }
           ant = dado1;
           printf("pos %4lld: 0x%02x\n",cont,0x00ff&ant);
           break;
        }
	}
    dado1 = fgetc(p1);
    cont++;
    printf("pos %4lld: 0x%02x\n",cont,0x00ff&dado1);
    cont2--;
    printf("\nQuantidade de byte do arquivo: %lld\nCabeçalho: 27\nDados compactados: %lld\nErros: %lld\n",cont,cont2,error);
    if( (ant == 255) && (dado1 == 217)){
	  printf("Done\n");
    }else
    printf("O arquivo apresentou um final inesperado.\n");
	fclose(p1);
	fclose(p2);
	system("pause");
}

/**************************
 *
 *
 **************************/
long long int contaBytesJls(char *nome)
{
	FILE *p;
	long long int cont=0;
	char dado;
	p = fopen(nome,"rb");
	if(p==NULL)
	{
		printf("Não foi passível abrir o arquivo: %s\n",nome);
		exit(1);
	}

	while(!feof(p))
	{
		dado = fgetc(p);
		if(cont < 24)
        {
		  printf("pos %4lld: 0x%0x\n",cont,(char)dado);
		}
		else if(cont == 24)
			printf("\n\n");
		//else if( cont <= 1000)
		//  printf("pos %5lld: 0x%0x\n",cont,(char)dado);
		cont++;
	}
    cont--;
	printf("\nQuantidade de byte do arquivo: %lld\nCabeçalho: 27\nDados compactados: %lld\n",cont,cont-27);
	return cont;
	fclose(p);
}

/*********************************************************
 *
 *
 *********************************************************/
void nextChar(FILE *p)
{
   look = fgetc(p);
}

/*********************************************************
 *
 *
 *********************************************************/
FILE *abreArq(char *nome, char *modo)
{
	FILE *p;
	p = fopen(nome, modo);
	if( p == NULL)
	{
		printf("Não foi possível abrir o arquivo: %s",nome);
		exit(1);
	}
	return p;
}

unsigned char toHex(char l, char r)
{
	unsigned char dado;
		switch (l)
	{
		case '0': dado = 0x00;break;
		case '1': dado = 0x10;break;
		case '2': dado = 0x20;break;
		case '3': dado = 0x30;break;
		case '4': dado = 0x40;break;
		case '5': dado = 0x50;break;
		case '6': dado = 0x60;break;
		case '7': dado = 0x70;break;
		case '8': dado = 0x80;break;
		case '9': dado = 0x90;break;
		case 'A': dado = 0xA0;break;
		case 'B': dado = 0xB0;break;
		case 'C': dado = 0xC0;break;
		case 'D': dado = 0xD0;break;
		case 'E': dado = 0xE0;break;
		case 'F': dado = 0xF0;break;
		default: 
		printf("formato invalido");
		system("pause");
	}
	switch (r)
	{
		case '0': dado = (dado|0x00);break;
		case '1': dado = (dado|0x01);break;
		case '2': dado = (dado|0x02);break;
		case '3': dado = (dado|0x03);break;
		case '4': dado = (dado|0x04);break;
		case '5': dado = (dado|0x05);break;
		case '6': dado = (dado|0x06);break;
		case '7': dado = (dado|0x07);break;
		case '8': dado = (dado|0x08);break;
		case '9': dado = (dado|0x09);break;
		case 'A': dado = (dado|0x0A);break;
		case 'B': dado = (dado|0x0B);break;
		case 'C': dado = (dado|0x0C);break;
		case 'D': dado = (dado|0x0D);break;
		case 'E': dado = (dado|0x0E);break;
		case 'F': dado = (dado|0x0F);break;
		default: 
		printf("formato invalido");
		system("pause");
	}
	return dado;
}
